# n-body

Repository for a N-Body simulation in collaboration with Julia

The simulation program is under `./nbody` 

The OpenGL based Visualization is under `./stargazer`
