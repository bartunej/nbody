# Nbody

The Bodies are specified in the Planets list. Right now the Sun and the major Planets of the solar system are the default system, with (hopefully) more to come. 

To build run:

```
    cmake . -DCMAKE_BUILD_TYPE=Release
    make
   
``` 
Execute with: 
```
 ./sim [number of steps] [delta T]
```
All of the locations will be printed to a file named output, as of right now the recording intervall has to be set in the source code. 
This program is able to calculate about 300 Million Steps of the solarsystem (Sun + 8 Planets) in about 90 seconds on an Intel i5-10300H.

