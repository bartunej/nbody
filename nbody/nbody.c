#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <sys/stat.h> 


#define pi 3.141592653589793
#define solar_mass 1.988e30

typedef struct {
    double x[3]; 
    double v[3]; 
    double a[3]; 
    double mass; 
} Planet; 
typedef struct {
    int noOfBodies; 
    int noOfSteps; 
    int noOfRecords;
    double *Records;
    int recordEvery;
    double dT; 
    Planet *Planets; 
    double initialEnergy;
} Sim;

//data from https://ssd.jpl.nasa.gov/horizons/app.html#/
//distances in AU 
// time in days

const double  g =  6.67430e-11;
const double AUinm  = 1/1.495978707e11;
const double M0 = 1.98846e30;
const double Yearins = 31556736.0f*31556736.0f;
const double Dayins = 86400.0*86400.0;
const double G = g* AUinm*AUinm*AUinm*M0*Dayins;
const double softening = 0.000;
Planet Planets[] = {
    {//SUN
        {-8.670573866903649E-03,  3.109791073898622E-03,  1.770795270853319E-04}, 
        {-3.064649754198640E-06, -8.502685754057324E-06,  1.404886158977827E-07},
        {0,0,0},
        1
    },
    {//MERCURY
        {-3.193064326780067E-01,  1.635684383629978E-01,  4.178316824233434E-02},
        {-1.869613626117531E-02, -2.381889384379678E-02, -2.309458181029623E-04},
        {0,0,0},
        3.302e23/M0
    },
    {//VENUS
        {-5.650377067587363E-01,  4.561446515741405E-01,  3.849931457555236E-02},
        {-1.285115045412401E-02, -1.579609568195534E-02,  5.248094066910089E-04},
        {0,0,0},
        48.685e23/M0
    },
    {//EARTH
        {-6.250208155167638E-01,  7.713345172147722E-01,  1.435517747544419E-04}, 
        {-1.370933795239888E-02, -1.084008953889242E-02,  1.337388517061019E-06},
        {0,0,0},
        5.97219e24/M0
     },  
     {//MARS    
        {-5.127066078534421E-01, -1.409766796258476E+00, -1.706970121270186E-02},
        {1.370314569272412E-02, -3.508692566376156E-03, -4.094278362287696E-04},
        {0,0,0},
         6.4171e23/M0
     },
     {//JUPITER
        {4.718554334409913E+00, -1.582526892577683E+00, -9.900030873833650E-02},
        {2.308958763570027E-03,  7.508456489339796E-03, -8.281404311737050E-05},
        {0,0,0},
        1.89818722e27/M0
    },
    {//SATURN
        {7.053251884525787E+00, -6.953182348577517E+00, -1.599189891098626E-01},  
        {3.604693773030273E-03,  3.960885344542721E-03, -2.121292992316584E-04},
        {0,0,0},
        5.6834e26/M0
    },
    {//URANUS
        {1.431272107973079E+01,  1.355709344414319E+01, -1.350723223401213E-01}, 
        {-2.733542252288362E-03,  2.672230146846226E-03,  4.521426676524279E-05},
        {0,0,0},
        86.813e24/M0
    },
    {//NEPTUNE
        {2.963598933768953E+01, -3.999671963918808E+00, -6.006262976908782E-01},
        {3.986151985601787E-04,  3.129296104369724E-03, -7.352181815621302E-05},
        {0,0,0},
        5.15e-5 
    }
      
    
  
};

static inline double distance( const double x1[3], const double x2[3]) {
    const double x = x1[0] - x2[0];
    const double y = x1[1] - x2[1];
    const double z = x1[2] - x2[2];
    
    return sqrt(x*x + y*y + z*z);
    
}

void printDebug(char *message) {
    printf("DEBUG: %s", message);
}
void printError(char *message) {
    printf("ERROR: %s", message);
}
const char welcome[] = "##################   Welcome to the n-body Simulator ##################\n"
"written by: Johannes and Julia\n\n\n";
double energy(const Planet Planets[], const int n){
    double e;
    int i=0, j=0, k=0;
    e = 0.0;

    for(i=0; i<n; i++){
        for(k=0; k<3; k++){
            e += 0.5 * Planets[i].mass * Planets[i].v[k] * Planets[i].v[k];
        }
        for(j=i+1; j < n; j++) {
            double  dist = distance(Planets[i].x, Planets[j].x);
            e -= (G * Planets[i].mass * Planets[j].mass) / dist;
        }
    }
    return e;
}

static inline double length(const double v[3]){
    const double vx = v[0];
    const double vy = v[1];
    const double vz = v[2];
    return sqrt(vx*vx+vy*vy+vz*vz);
}

double an_mom(int n){
    double L = 0;

    for(int i=0; i<n; i++){
         double v = length(Planets[i].v);
         double Origin[3] = {0,0,0};
         double r = distance(Planets[i].x, Origin);
         L+= Planets[i].mass * v * r;
    }
    return L;
}
void printResults(char *filename, double *recordPositions, int print, int noOfBodies);

int main(int argc, char * argv[]) {
    printf("%s\n", welcome);
    Sim sim; 
    
    if (argc == 3) {
        sim.noOfSteps = atoi(argv[1]);
        sim.dT = atof(argv[2]); 
    } else {
        printError("No Number of Steps or Timestep specified.......FIX IT\n");
        exit(EXIT_FAILURE);
    }
    
    sim.noOfBodies = sizeof(Planets)/sizeof(Planet); 
    sim.recordEvery = 500;
    sim.Planets = Planets;   
    sim.initialEnergy = energy(sim.Planets, sim.noOfBodies);
    
    //allocate memory for the calculated data
    sim.Records = (double*)malloc(3* sizeof(double)*sim.noOfBodies*(int) sim.noOfSteps/sim.recordEvery);
    if (sim.Records == NULL) {
        printError("Couldn't  mallocate memory....exiting");
        exit(EXIT_FAILURE);
    }
    
    
    sim.noOfRecords = 0;
    long counter = 1;
    Planet *Planets = sim.Planets;
    double *Records = sim.Records;
    double dT = sim.dT;
    printf("ddT:: %lf\n", sim.dT);
    while (counter < sim.noOfSteps) {
        
        for (int l = 0; l < sim.noOfBodies; l++) {
            Planets[l].v[0] += 0.5 * Planets[l].a[0]* dT;
            Planets[l].v[1] += 0.5 * Planets[l].a[1]* dT;
            Planets[l].v[2] += 0.5 * Planets[l].a[2]* dT;
            Planets[l].a[0] = 0;
            Planets[l].a[1] = 0;
            Planets[l].a[2] = 0;
            
        }
        
        if (counter % sim.recordEvery == 0) {
            int local = sim.noOfRecords*3*sim.noOfBodies;
            for (int k = 0; k < sim.noOfBodies; k++) {
                Planets[k].x[0] += dT * Planets[k].v[0];
                Planets[k].x[1] += dT * Planets[k].v[1];
                Planets[k].x[2] += dT * Planets[k].v[2];
                Records[local+k*3] = Planets[k].x[0]; 
                Records[local+k*3+1] = Planets[k].x[1];
                Records[local+k*3+2] = Planets[k].x[2];
                
            }
            
            sim.noOfRecords++;
            
        } else {
             for (int k = 0; k < sim.noOfBodies; k++) {
                Planets[k].x[0] += dT * Planets[k].v[0];
                Planets[k].x[1] += dT * Planets[k].v[1];
                Planets[k].x[2] += dT * Planets[k].v[2];
                
            }
        }
        
        for (int i = 0; i < sim.noOfBodies; i++) {
            for (int j = i+1; j < sim.noOfBodies; j++) {
                    register double  dist = distance(Planets[i].x, Planets[j].x);
                    register double a = G/((dist*dist*dist)+softening);
                    
                    Planets[i].a[0] += a * Planets[j].mass * ( Planets[j].x[0] - Planets[i].x[0] );
                    Planets[i].a[1] += a * Planets[j].mass * ( Planets[j].x[1] - Planets[i].x[1] );
                    Planets[i].a[2] += a * Planets[j].mass * ( Planets[j].x[2] - Planets[i].x[2] );
                    Planets[j].a[0] += a * Planets[i].mass * ( Planets[i].x[0] - Planets[j].x[0] );
                    Planets[j].a[1] += a * Planets[i].mass * ( Planets[i].x[1] - Planets[j].x[1] );
                    Planets[j].a[2] += a * Planets[i].mass * ( Planets[i].x[2] - Planets[j].x[2] );
            }
        }
        
            
        for (int m = 0; m < sim.noOfBodies; m++) {
            Planets[m].v[0] += 0.5 * Planets[m].a[0]* dT;
            Planets[m].v[1] += 0.5 * Planets[m].a[1]* dT;
            Planets[m].v[2] += 0.5 * Planets[m].a[2]* dT;
            
        }
        counter++;
        
    }
   
    
    double deltaEnergy = (energy(sim.Planets,sim.noOfBodies)-sim.initialEnergy)/sim.initialEnergy;
    printf("energy error: %.25e\n", deltaEnergy);
    printf("total angular momentum after: %f\n", an_mom(sim.noOfBodies));
    
    char * outputFile = "output"; 
    printResults(outputFile, sim.Records, sim.noOfRecords, sim.noOfBodies);
    
    free(sim.Records);
}
//this function is for printing the result to a file
void printResults(char *filename, double *recordPositions, int print, int noOfBodies) {

    FILE *output = fopen(filename, "w+"); 
    if (output == NULL) {
        printf("Couldn't open output file: %s", filename);
        exit(EXIT_FAILURE);
    } 
    
    fprintf(output, "#v1\n%d\n%d\n", print ,noOfBodies );
    for (int i = 0; i < print; i++) {
        int offset = noOfBodies*3*i;
        for (int body = offset; body < offset+noOfBodies*3; body+=3) {
            fprintf(
                output,     
                "%.12e %.12e %.12e ", 
                recordPositions[body],
                recordPositions[body+1],
                recordPositions[body+2]
            );
        }
        fprintf(output, "\n");
        
    }
    
    fclose(output);
    
}

