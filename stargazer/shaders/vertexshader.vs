#version 330 core
layout (location = 1) in vec3 aPos;
//layout (location = 16) in float
//out vec4 vertexColor;


uniform mat4 view;
uniform mat4 proj;
uniform float worldScale;
uniform float pointScale;
void main()
{
   mat4 scaling = mat4(worldScale);
   scaling[3][3] = 1.0f;
   vec4 World =  proj*inverse(view)*scaling*vec4(aPos, 1.0);
   gl_PointSize = 1/(World.z)*pointScale;
   
   gl_Position =  World;
   //vertexColor = vec4(1,1,1,0.5);  
   
   
}
