#version 330 core
layout (location = 0) in vec3 aPos;
out vec4 vertexColor;

uniform mat4 view;

void main()
{
   gl_Position =  vec4(aPos, 1.0);
   gl_PointSize = 30;
   vertexColor = vec4(0.45,0.85,0.88,1.0);  
   
   
}
