#version 450 core


out vec4 FragColor;
uniform bool Trace; 
 
void point(in vec2 gl_PointCoord, out vec4 FragColor) {
    vec2 center = gl_PointCoord-vec2(0.5,0.5);
    float radius = dot(center, center);
    if (radius > 0.25) 
    {   
        gl_FragDepth = 0.999;
        discard;
    } if ( radius <0.13) 
    {
        FragColor = vec4(1.0,1.0,1.0,1.0);
    } else 
    {
        float brightness = -4 * (radius-0.13) +0.8;
        FragColor = vec4(brightness, brightness, brightness, 0.5);
    }
    
}
void trace(in vec2 gl_PointCoord, out vec4 FragColor) {
    
    
    FragColor = vec4(0.84,0.36,0.055,1.0);
    
}

void main()
{
    gl_FragDepth = gl_FragCoord.z;
    if (Trace) {
        trace(gl_PointCoord, FragColor);
    } else {
         point(gl_PointCoord, FragColor);
     }
}
