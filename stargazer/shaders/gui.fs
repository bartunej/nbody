#version 330 core

out vec4 FragColor;
in vec4 vertexColor;
in float depth;


void main()
{
   FragColor = vertexColor;
}
