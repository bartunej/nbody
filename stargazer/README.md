# Welcome to Stargazer

This is the visualization portion of this repository. 
Fly around the scene and study the trajectory of your system. 

# Build and Run

This program has been tested to work with Fedora35 so your mileage may vary. Feel free to open an issue or PR, if you can not get it to run on your platform. 

First of all you need to run include the neccesay 

```
cmake . -DCMAKE_BUILD_TYPE=Release
make 
./stargazer <name-to-logfile>
```

You can find samples in the `/samples` directory. 

# Controls 

`SPACE` Pause/ Unpause 

`.`     Speed up time

`,`     Slow/ Reverse time

`c`     Toggle trajectory trace

`n/ m`     Shorter/ Longer trace

`k/ l`  Scale the scene

`o/ p`  Scale the size of the objects

`right/ left arrows` Increase/ Decrease movement speed

Use the mouse/ touchpad to look around. 

# Screenshots
![](screenshot/solar_system.png "")

![](screenshot/chaosI.png "")

![](screenshot/chaosII.png "")




