#ifndef MAIN
#define MAIN

#include "importData.h"
#include "Camera.h"

enum textureFormat {png, jpg};
struct texture{
    char* name;
    enum textureFormat format;
    bool flip;
};

void processInput( 
    GLFWwindow *window, 
    int key,
    int scancode,
    int action,
    int mods
);
    
void updateCamera();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);    
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void compileAndAttachShaders( unsigned int Program,char vertexSource[], char fragmentSource[]);

void loadTexture(struct texture Tex) {

    int width, height, nrChannels; 
    stbi_set_flip_vertically_on_load(Tex.flip);
    
    unsigned char* data = stbi_load(Tex.name, &width, &height, &nrChannels, 0);
    if (data) {
    
        switch (Tex.format) {
            case jpg:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
                break;
            case png: 
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); 
                break;       
        }        
        
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        printf("Failed to load texture");
        exit(EXIT_FAILURE);
    }
    stbi_image_free(data);

}
#endif
