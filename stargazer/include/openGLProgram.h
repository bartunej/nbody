#include <glad/glad.h>

#ifndef openGLProgram 
#define openGLProgram
void compileAndAttachShaders( unsigned int Program, char vertexSource[], char fragmentSource[]) {
    
    unsigned int vertexShader, fragmentShader;
    char vertexbuffer[2024], fragmentbuffer[2024], infoLog[512];
    int success; 
    long charsRead;
    
    FILE* SourceFile = fopen(vertexSource, "r");
    if (SourceFile) {
        charsRead = fread(vertexbuffer,sizeof(char),  sizeof(vertexbuffer)/sizeof(char), SourceFile);
        vertexbuffer[charsRead] = '\0';
        
        const char* vertexptr = &vertexbuffer[0];
        
        fclose(SourceFile);
        if ( charsRead > 0&& charsRead <  sizeof(vertexbuffer)/sizeof(char)) {
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &vertexptr, NULL);
        } else {
            printf("Failed to open %s as vertex Source File\n", vertexSource);
            exit(EXIT_FAILURE);
        }
    } else {
        printf("Failed to open file: %s\n", vertexSource);
        exit(EXIT_FAILURE);
    }
    glCompileShader(vertexShader);
    
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n");
        printf("%s\n", infoLog);
         printf("\n\n%s", vertexbuffer);
        return exit(EXIT_FAILURE);
    } 
    
    
    SourceFile = fopen(fragmentSource, "r");
    if (SourceFile) {
        charsRead = fread(fragmentbuffer,sizeof(char),  sizeof(fragmentbuffer)/sizeof(char), SourceFile);
        fragmentbuffer[charsRead] = '\0';
        const char* fragmentptr = &fragmentbuffer[0];
        fclose(SourceFile);
        if (charsRead > 0 && charsRead <  sizeof(fragmentbuffer)/sizeof(char)) {
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &fragmentptr, NULL);
        } else {
            printf("Failed to open %s as vertex Source File\n", fragmentSource);
            exit(EXIT_FAILURE);
        }
    } else {
        printf("Failed to open file: %s\n", fragmentSource);
        exit(EXIT_FAILURE);
    }        
    glCompileShader(fragmentShader);
    
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n");
        printf("%s\n", infoLog);
        printf("\n\n%s", fragmentbuffer);
        return exit(EXIT_FAILURE);
    } 
    
    glAttachShader(Program, vertexShader);
    glAttachShader(Program, fragmentShader);
    glLinkProgram(Program);
    
    glGetProgramiv(Program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(Program, 512, NULL, infoLog);
        printf("ERROR::GL::Program::LINKING_FAILES\n");
        printf("%s\n", infoLog);
        return exit(EXIT_FAILURE);
    } 
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

#endif
