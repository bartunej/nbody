#ifndef CAMERA
#define CAMERA
//#include <threads.h>


typedef struct {
    graphene_vec3_t position;
    graphene_vec3_t direction;
    graphene_vec3_t left;
    graphene_vec3_t up;
    graphene_matrix_t view;
    float phi;
    float theta;
    float fov;
    float speed;
    float sensitivity;
    unsigned int program;
    float pointScale;
    float worldScale;
} Camera;

void CameraInit(Camera *cam, unsigned int program) {
    cam->phi = -90; 
    cam->theta = 0;
    cam->fov = 55;
    cam->speed = 3.0; 
    cam->sensitivity = 0.0005;
    cam->program = program;
    cam->pointScale = 415;
    cam->worldScale = 1.0f;
}
#endif
