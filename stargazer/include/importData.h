#ifndef SIMDATA
#define SIMDATA
typedef struct {
    float x;
    float y;
    float z;
} Vector;

typedef struct {
    long noOfBodies; 
    long noOfRecords;
    float *allocator;
    float *traceAllocator;
    char *filename;
    long size;
    bool run; 
    int speed;
    bool trace;
    unsigned int *indices; 
    unsigned int *count;
    unsigned int noOfTracepoints;
} SimData; 

void importData(SimData *sim) {

    char *charptr1, *charptr2; 
    FILE *output = fopen(sim -> filename, "r"); 
    
    //get the file size to make sure we have enough space to read it!
    struct stat filestats; 
    stat(sim->filename, &filestats); 
    long size = filestats.st_size; //since we read line by line this is large enough!
    //buffer is freed at the end of this scope
    char* buffer = (char*)malloc(size); 
    
    //random line ignore
    getline(&buffer, &size, output);
    printf("Vertex File opened: %s\n", buffer);
    
    //this line has information about the number of Record Positions
    getline(&buffer, &size, output);
    charptr1 = &buffer[0];
    sim->noOfRecords = strtol(charptr1, &charptr2, 0); 
    printf("number of Recorded Points: :%ld\n",  sim -> noOfRecords);
    
    //this line tells us how many bodies are part of the system
    getline(&buffer, &size, output);
    charptr1 = &buffer[0];
    sim -> noOfBodies = strtol(charptr1, &charptr2, 0); 
    printf("number of Bodies:::%ld\n", sim -> noOfBodies);
    
    //now we can use the noOfBodies and the NoOfRecords to allocate enough memory to 
    //hold all the positions in memory 
    //pointer is owned by caller and will be freed when the program terminates
    sim -> size =  sim -> noOfRecords * sim -> noOfBodies*3;
    sim -> allocator = malloc(size*sizeof(float));
    if (!sim->allocator) {
        exit(1);
    }
    sim -> traceAllocator = malloc(size*sizeof(float));
    if (!sim->traceAllocator) {
        exit(1);
    }
    charptr1 = &buffer[0];
   
    for (long record = 0; record < sim->noOfRecords; record++) {
        long offset = record * 3*sim -> noOfBodies;
        long offset_trace = sim->noOfRecords;
        
        getline(&buffer, &size, output);
        charptr1 = &buffer[0];
        for (long body = 0; body <sim -> noOfBodies*3; body+=3) {
            sim->allocator[offset + body] = strtof(charptr1, &charptr2);
            sim->allocator[offset + body+1] = strtof(charptr2, &charptr2);
            sim->allocator[offset + body+2] = strtof(charptr2, &charptr2);
            sim->traceAllocator[body*offset_trace +record*3] = sim->allocator[offset + body];
            sim->traceAllocator[body*offset_trace +1+record*3] = sim->allocator[offset + body+1];
            sim->traceAllocator[body*offset_trace +2+record*3] = sim->allocator[offset + body+2];
            charptr1 = charptr2;
        }
        
    }
    free(buffer);
}
#endif
