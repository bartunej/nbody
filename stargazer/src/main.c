//open gl related libraries
#include <glad/glad.h>
#include <GLFW/glfw3.h>

//libc imports
#include <stdio.h> 
#include <stdbool.h>
#include <math.h> 
#include <sys/stat.h>
#include <threads.h> 

//image library for importing pictures
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

//linear algebra library
#include <graphene-1.0/graphene.h> 

//project specific files
#include "main.h"
#include "importData.h"
#include "openGLProgram.h"


float pause_indicator[] = {
    //first triangle
     
     0.90f,  0.98f, -1.0,  // top right
     0.94f, 0.8f, -1.0,  // bottom right
     0.90f, 0.8f, -1.0,
     
     0.90f,   0.98f, -1.0,  // top right
     0.94f,  0.98f, -1.0,  // bottom right
     0.94f, 0.8f, -1.0,
     
     0.95f,   0.98f, -1.0,  // top right
     0.99f, 0.8f, -1.0,  // bottom right
     0.95f, 0.8f, -1.0,
     
     0.95f,   0.98f, -1.0,  // top right
     0.99f,  0.98f, -1.0,  // bottom right
     0.99f, 0.8f, -1.0,
     
     
};

float play_indicator[] = {
     0.9f,  1.0f, -1.0,  // top right
     1.0f, 0.9f, -1.0,  // bottom right
     0.9f, 0.8f, -1.0,   // bottom left
};
 
unsigned int indices[] = {
    0, 1, 3,    //first  triangle
    1, 2, 3     //second triangle
}; 

float deltaTime = 0.0f; // Time between current frame and last frame
float lastFrame = 0.0f;
float lastX = 0, lastY = 0; 

graphene_vec3_t direction; 
Camera cam;  
SimData sim;
GLFWwindow *window;


void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam ) {
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}

typedef struct {
    float distance; 
    unsigned int index; 
} TransparentPoint;

int main(int argc, char *argv[]) {
    
    glfwInit();
     
    char *filename = argv[1];
    
    sim.filename = filename;  
    sim.allocator = NULL;
    sim.run = false;
    sim.speed = 1;
    sim.noOfTracepoints = 1000;
    sim.trace = false;
    importData(&sim);
    TransparentPoint *root = malloc(sizeof(TransparentPoint) * (sim.noOfBodies));
    if (!root) {
        exit(1);
    }
    
    
    
    //init glfw and configure the enum that is the state of glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES,4);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
   
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    window = glfwCreateWindow(1000,1000,"Visualize", NULL, NULL);
    if (window == NULL) {
        printf("Failed to create GLFW window");
        glfwTerminate();
        return -1; 
    } 
    glfwSetKeyCallback(window, processInput);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  
    
    glfwMakeContextCurrent(window);
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("failed to initialize GLAD");
        return -1;
    }
    
    glViewport(0, 0, 1000, 1000);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glEnable( GL_DEBUG_OUTPUT );
    glEnable(GL_MULTISAMPLE);
    glDebugMessageCallback( MessageCallback, 0 );
    
    //to change things related to GL_POINT Primitive
    glEnable(GL_PROGRAM_POINT_SIZE);
    
    
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(3.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    unsigned int shaderProgram; 
    shaderProgram = glCreateProgram();
    
	//Prepare the shaders
    compileAndAttachShaders( shaderProgram, "./shaders/vertexshader.vs", "./shaders/fragmentshader.fs");
    glUseProgram(shaderProgram);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    unsigned int VBO, EBO, VAO;
    glGenVertexArrays(1,&VAO);
    glGenBuffers(1,&EBO);
    glGenBuffers(1, &VBO);
    
    //after binding the VAO can be used
    glBindVertexArray(VAO);
    
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sim.size*sizeof(float), sim.allocator, GL_DYNAMIC_DRAW);
    
    glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(1);
     unsigned int VAO_trace, VBO_trace;
    glGenVertexArrays(1,&VAO_trace);
    glGenBuffers(1,&VBO_trace);
    
    //after binding the VAO can be used
    glBindVertexArray(VAO_trace);
    
    glBindBuffer(GL_ARRAY_BUFFER, VBO_trace);
    int offset = 1;
    glBufferData(GL_ARRAY_BUFFER, sim.size*sizeof(float), sim.traceAllocator, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE,offset *3* sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    
   
    float arr[16]; 
    graphene_matrix_t models;
    graphene_vec3_t vector;
    graphene_matrix_t projection_mat;
    graphene_matrix_t* projection = graphene_matrix_init_perspective(&projection_mat, 45,1,0.1,10000.0);
    graphene_matrix_to_float(projection, arr);
    
    glUniformMatrix4fv(glGetUniformLocation( shaderProgram, "proj"), 1, GL_FALSE, arr);
    
    int t = 0;  
    graphene_point3d_t point; 
     
    graphene_vec3_t* eye =graphene_vec3_init(&cam.position, 0.0, 0.0, 3);
    
    CameraInit(&cam, shaderProgram);
    glfwSetCursorPos(window, 0, 0);  
    glUniform1f(glGetUniformLocation(cam.program, "pointScale"), cam.pointScale);

    unsigned int gui; 
    gui = glCreateProgram();
    
    compileAndAttachShaders( gui, "./shaders/gui.vs", "./shaders/gui.fs");
    glUseProgram(gui); 
    unsigned int VBO_play, VAO_play, VBO_pause, VAO_pause; 
    glGenVertexArrays(1,&VAO_play);
    glGenVertexArrays(1,&VAO_pause);
    glGenBuffers(1,&VBO_play);
    glGenBuffers(1,&VBO_pause);
    
    glBindVertexArray(VAO_play); 
    glBindBuffer(GL_ARRAY_BUFFER,VBO_play); 
    glBufferData(GL_ARRAY_BUFFER, 9*sizeof(float), play_indicator, GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(VAO_pause); 
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pause); 
    glBufferData(GL_ARRAY_BUFFER,36*sizeof(float), pause_indicator, GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    graphene_vec4_t zvec; 
    graphene_vec4_t vertex;
    int trace = sim.noOfTracepoints;
    while (!glfwWindowShouldClose(window) && t < sim.noOfRecords) {
        //empty screen
        glDepthMask(GL_TRUE);
        glClear(GL_COLOR_BUFFER_BIT);
        glClear(GL_DEPTH_BUFFER_BIT);
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        
        //render bodies
        glUseProgram(shaderProgram);
        glfwPollEvents();
        updateCamera();
        graphene_matrix_get_row(&cam.view, 2, &zvec);
        
        for (int k = 0; k < sim.noOfBodies; k++) {
            graphene_vec4_t *tmp = graphene_vec4_init(
                &vertex, 
                sim.traceAllocator[t+k*sim.noOfRecords*3],
                sim.traceAllocator[t+k*sim.noOfRecords*3+1],
                sim.traceAllocator[t+k*sim.noOfRecords*3+2],
                1.0f);
            
            root[k].distance = graphene_vec4_dot(&vertex, &zvec);
            root[k].index = k;
        }
        glBindVertexArray(VAO_trace);
        glUniform1f(glGetUniformLocation(cam.program, "Trace"), 0);
        for (int body = 0; body < sim.noOfBodies; body++) {
            glDrawArrays(GL_POINTS, t+body*sim.noOfRecords,1);
        }
        int trace = sim.noOfTracepoints;
        if (sim.trace == true) {
            glBindVertexArray(VAO_trace);
            glUniform1f(glGetUniformLocation(cam.program, "Trace"), 1);
           
            for (int j = 0; j <sim.noOfBodies; j++) {
                glDrawArrays(
                    GL_LINE_STRIP, 
                    sim.noOfRecords*j/1+ fmax(0, t-trace),
                   fmin(t/1, trace)
                );
            }
        }
        
        //render gui       
        glUseProgram(gui);
        glBindVertexArray(VAO_play);
        if (sim.run) {
            t+= sim.speed;
            glBindVertexArray(VAO_play);
            
        } else {
            glBindVertexArray(VAO_pause);
            sim.run = 0;
        }
        if (t == 0 && sim.speed < 0) {
            sim.run = 0;
            sim.speed = 1;
        }
        glDrawArrays(GL_TRIANGLES, 0,3+9*(1-sim.run));
        
        //swap
        glfwSwapBuffers(window);
    }
    glfwTerminate();
    free(root);
    return 0;
}

void processInput( 
    GLFWwindow *window, 
    int key,
    int scancode,
    int action,
    int mods
    ) {
   
    if (key ==GLFW_KEY_SPACE && action == GLFW_PRESS) {
        sim.run = -1*(sim.run -1);
    } 
    
    else if (key == GLFW_KEY_COMMA && action == GLFW_PRESS) {
        sim.speed -= 1; 
    }
    else if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS) {
        sim.speed += 1; 
    }
    else if (key == GLFW_KEY_C && action == GLFW_PRESS) {
        sim.trace = (sim.trace-1)*-1; 
    }
    else if (key == GLFW_KEY_M && action == GLFW_PRESS) {
        sim.noOfTracepoints =(int)sim.noOfTracepoints* 2; 
    }
    else if (key == GLFW_KEY_N && action == GLFW_PRESS) {
        sim.noOfTracepoints = (int)sim.noOfTracepoints/2; 
    }
    
    
}

void updateCamera () {
    //acquire the lock to write to the fields of the Camera
    float arr[16];
     double xpos, ypos;
    graphene_vec3_t tmp;
    
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame; 
    printf("Frametime : %lf\n", deltaTime);
    lastFrame = currentFrame;
   
    float speed = cam.speed*2;
    glfwGetCursorPos(window, &xpos, &ypos);
    glfwSetCursorPos(window, 0,0 );  
    
    cam.phi += cam.sensitivity *(float)(-xpos);
    cam.theta += cam.sensitivity * (float)(ypos  );
    graphene_vec3_t *left = graphene_vec3_init(
        &cam.left,
        cos(cam.phi),
        0,
        -sin(cam.phi)
    );
    
    graphene_vec3_cross(&cam.left, &cam.direction, &cam.up);
    graphene_vec3_negate(&cam.up, &cam.up);
    
    
    
    graphene_vec3_t *direction = graphene_vec3_init(
        &cam.direction, 
        sin(cam.phi) * cos(cam.theta),
        sin(cam.theta),
        cos(cam.phi) * cos(cam.theta)
        
    );
    
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
       
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        graphene_vec3_scale( &cam.direction,(speed *1.2)* deltaTime, &tmp);
        graphene_vec3_add(&cam.position, &tmp, &cam.position);
       
        
    }   
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        graphene_vec3_scale( &cam.direction, (speed *1.2)* deltaTime, &tmp);
        graphene_vec3_subtract(&cam.position, &tmp, &cam.position);
        
    }  
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        graphene_vec3_scale( &cam.left, speed * deltaTime, &tmp);
        graphene_vec3_add(&cam.position, &tmp, &cam.position);
    } 
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        graphene_vec3_scale(&cam.left, speed * deltaTime, &tmp);
        graphene_vec3_subtract(&cam.position, &tmp, &cam.position);
    } 
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
        graphene_vec3_scale( &cam.up, speed * deltaTime, &tmp);
        graphene_vec3_add(&cam.position, &tmp, &cam.position);
    } 
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        graphene_vec3_scale( &cam.up, speed * deltaTime, &tmp);
        graphene_vec3_subtract(&cam.position, &tmp, &cam.position);
    } 
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        cam.pointScale *= 1.05;
        glUniform1f(glGetUniformLocation(cam.program, "pointScale"), cam.pointScale);
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
        cam.pointScale /= 1.05;
        glUniform1f(glGetUniformLocation(cam.program, "pointScale"), cam.pointScale);
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        cam.speed  *=1.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        cam.speed  *=0.9f;
    }
    if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
        cam.worldScale *= 1.1f; 
    }
    if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
        cam.worldScale /= 1.1f; 
    }
    graphene_matrix_t worldScale; 
    graphene_matrix_t *worldScale_pointer = graphene_matrix_init_scale(
        &worldScale,
        cam.worldScale,
        cam.worldScale,
        cam.worldScale
    );
    graphene_matrix_to_float(&worldScale, arr);
    glUniform1f(glGetUniformLocation(cam.program, "worldScale"), cam.worldScale);
        
    
   
    graphene_vec3_subtract(&cam.position, &cam.direction, &tmp);

    graphene_matrix_t* view = graphene_matrix_init_look_at(
            &cam.view, 
            &cam.position, 
            &tmp, 
            &cam.up
    );
    
    
    graphene_matrix_to_float(&cam.view, arr);
    glUniformMatrix4fv(glGetUniformLocation( cam.program, "view"), 1, GL_FALSE, arr);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    
    glViewport(0,0,1800,1800);
}
